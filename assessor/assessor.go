package assessor

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"proxy-miner/models"
	"strings"
	"sync"
	"time"
)

const (
	httpbin = "https://httpbin.org/ip"
	timeout = models.RequestTimeout * time.Second
)

type Resp struct {
	Origin string `json:"origin"`
}

func Start(db *gorm.DB) {
	go func() {
		for {
			checkAndDeleteRaw(db)
			time.Sleep(time.Second)
		}
	}()

	for {
		checkAndUpdateProxy(db)
		time.Sleep(time.Second)
	}
}

func checkAndDeleteRaw(db *gorm.DB) {
	wg := &sync.WaitGroup{}
	rawProxies := []*models.RawProxy{}

	db.Where("update_time <= ?", time.Now().Unix()-int64(60)).
		Order("update_time").
		Limit(100).
		Find(&rawProxies)

	if len(rawProxies) == 0 {
		time.Sleep(time.Second)
		return
	}

	idList := []int64{}

	for _, r := range rawProxies {
		idList = append(idList, r.ID)
	}

	db.Where("id in (?)", idList).Delete(&models.RawProxy{})

	for _, rawProxy := range rawProxies {
		wg.Add(1)
		go checkProxy(db, rawProxy.Content, wg)
	}
	wg.Wait()
}

func checkAndUpdateProxy(db *gorm.DB) {
	wg := &sync.WaitGroup{}
	proxies := []*models.Proxy{}

	db.Where("update_time <= ?", time.Now().Unix()-int64(60)).
		Order("update_time").
		Limit(200).
		Find(&proxies)

	if len(proxies) == 0 {
		time.Sleep(time.Second)
		return
	}

	for _, proxy := range proxies {
		wg.Add(1)
		go checkProxy(db, proxy.Content, wg)
	}
	wg.Wait()
}

func checkProxy(db *gorm.DB, Content string, wg *sync.WaitGroup) {
	defer wg.Done()

	timestamp := time.Now().UnixNano() / 1e6
	isOK := true

	errs := make(chan bool, 3)
	w := &sync.WaitGroup{}

	for i := 0; i < 3; i++ {
		w.Add(1)
		go doRequest(Content, w, errs)
	}
	w.Wait()
	close(errs)

LoopErrs:
	for val := range errs {
		if !val {
			isOK = false
			break LoopErrs
		}
	}

	timeCost := float64(time.Now().UnixNano()/1e6-timestamp) / 1e3

	err := models.UpdateProxy(db, Content, isOK, timeCost)
	if err != nil {
		log.Printf("[Assessor] %s", err)
		return
	}
}

func doRequest(Content string, wg *sync.WaitGroup, errs chan bool) {
	defer wg.Done()

	u, err := url.Parse("http://" + Content)
	if err != nil {
		log.Printf("[Assessor] %s, %s", Content, err)
		errs <- false
		return
	}

	transport := &http.Transport{
		Proxy: http.ProxyURL(u),
	}
	transport.TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	transport.DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
		conn, err := net.DialTimeout(network, addr, timeout)
		if err != nil {
			return nil, err
		}
		_ = conn.SetDeadline(time.Now().Add(timeout))
		return conn, nil
	}

	client := &http.Client{
		Transport: transport,
	}

	request, err := http.NewRequest("GET", httpbin, nil)
	if err != nil {
		log.Printf("[Assessor] %s", err)
		errs <- false
		return
	}

	response, err := client.Do(request)
	if err != nil {
		log.Printf("[Assessor] %s", err)
		errs <- false
		return
	}

	defer response.Body.Close()

	bodyBytes, err := ioutil.ReadAll(response.Body)

	if err != nil {
		log.Printf("[Assessor] %s", err)
		errs <- false
		return
	}

	resp := Resp{}

	err = json.Unmarshal(bodyBytes, &resp)

	if err != nil {
		log.Printf("[Assessor] %s", err)
		errs <- false
		return
	}

	contains := strings.Contains(resp.Origin, strings.Split(Content, ":")[0])

	errs <- contains && response.StatusCode == 200
}
