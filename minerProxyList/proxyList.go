package minerProxyList

import (
	"context"
	"encoding/json"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"proxy-miner/models"
	"sync"
	"time"
)

const (
	baseUrl = "https://www.proxy-list.download/api/v0/get?l=en&t="
	httpbin = "https://httpbin.org/ip"
	timeout = models.RequestTimeout * time.Second
)

var (
	protos = []string{"http", "https"}
)

type Proxy struct {
	IP   string `json:"IP"`
	PORT string `json:"PORT"`
}

func (p *Proxy) content() string {
	return p.IP + ":" + p.PORT
}

type ListProxies struct {
	LISTA []Proxy `json:"LISTA"`
}

func Start(db *gorm.DB) {
	for {

		wg := &sync.WaitGroup{}

		for _, proto := range protos {
			wg.Add(1)
			go checkAndCreate(proto, db, wg)
		}

		wg.Wait()
	}
}

func checkAndCreate(proto string, db *gorm.DB, wg *sync.WaitGroup) {
	defer wg.Done()
	sem := make(chan bool, 30)

	rawProxies := getProxyList(baseUrl + proto)

	if rawProxies == nil {
		return
	}

	for _, proxy := range rawProxies {
		wg.Add(1)
		sem <- true

		go checkProxy(db, proxy.content(), wg, sem, proto)
	}
}

func checkProxy(db *gorm.DB, proxyContent string, wg *sync.WaitGroup, sem chan bool, proto string) {
	defer wg.Done()
	defer func() {
		<-sem
	}()

	type Resp struct {
		Origin string `json:"origin"`
	}

	u, err := url.Parse("http://" + proxyContent)
	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl+proto, err)
		return
	}

	transport := &http.Transport{
		Proxy: http.ProxyURL(u),
	}

	transport.DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
		conn, err := net.DialTimeout(network, addr, timeout)
		if err != nil {
			return nil, err
		}
		_ = conn.SetDeadline(time.Now().Add(timeout))
		return conn, nil
	}

	client := &http.Client{
		Transport: transport,
	}

	request, err := http.NewRequest("GET", httpbin, nil)
	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl+proto, err)
		return
	}

	response, err := client.Do(request)
	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl+proto, err)
		return
	}

	defer response.Body.Close()

	resp := Resp{}

	decoder := json.NewDecoder(response.Body)
	err = decoder.Decode(&resp)

	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl+proto, err)
		return
	}
	models.CreateRawProxy(db, proxyContent)
}

func getProxyList(link string) []Proxy {
	request, err := http.NewRequest("GET", link, nil)
	if err != nil {
		log.Printf("[Miner][%s] %s", link, err)
		return nil
	}

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		log.Printf("[Miner][%s] %s", link, err)
		return nil
	}

	defer response.Body.Close()
	if err != nil {
		log.Printf("[Miner][%s] %s", link, err)
		return nil
	}

	defer response.Body.Close()

	resp := []ListProxies{}

	body, err := ioutil.ReadAll(response.Body)

	err = json.Unmarshal(body, &resp)

	if err != nil {
		log.Printf("[Miner][%s] %s", link, err)
		return nil
	}
	return resp[0].LISTA
}
