package main

import (
	"flag"
	"log"
	"proxy-miner/assessor"
	"proxy-miner/minerChina"
	"proxy-miner/minerProxyList"
	"proxy-miner/minerWebanetlabs"
	"runtime"
	"time"
)

func main() {
	runtime.GOMAXPROCS(runtime.NumCPU())

	var mode string
	flag.StringVar(&mode, "mode", "all", "all/miner/assessor, default is all")
	flag.Parse()

	log.Printf("Operating Mode: %s, will start running after 3 seconds", mode)

	time.Sleep(3 * time.Second)

	switch mode {
	case "all":
		db, err := NewPostgresql()
		if err != nil {
			log.Fatalln(err)
		}
		defer db.Close()
		go minerWebanetlabs.Start(db)
		go minerChina.Start(db)
		go assessor.Start(db)

	case "assessor":
		db, err := NewPostgresql()
		if err != nil {
			log.Fatalln(err)
		}
		defer db.Close()
		go assessor.Start(db)

	case "miner":
		db, err := NewPostgresql()
		if err != nil {
			log.Fatalln(err)
		}
		defer db.Close()
		go minerWebanetlabs.Start(db)
		go minerChina.Start(db)
		go minerProxyList.Start(db)

	default:
		log.Panicf("Unknown mode: %s", mode)
	}
	select {}
}
