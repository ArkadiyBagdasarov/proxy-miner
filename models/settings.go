package models

const (
	RequestTimeout              = 5
	UserAgent                   = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"
	AssessorAllowSuccessRateMin = 0.5
)
