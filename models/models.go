package models

import (
	"github.com/jinzhu/gorm"
	"log"
	"math"
	"strings"
	"time"
)

type RawProxy struct {
	ID         int64  `gorm:"index;primary_key" json:"id"`
	IP         string `json:"ip"`
	Port       string `json:"port"`
	Content    string `gorm:"unique_index:unique_raw_content;" json:"content"`
	UpdateTime int64  `json:"update_time"`
}

func CreateRawProxy(db *gorm.DB, Content string) {
	raw := &RawProxy{}

	ipPort := strings.Split(Content, ":")
	now := time.Now().Unix()

	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	tx.Where(RawProxy{Content: Content}).Attrs(
		RawProxy{
			IP:         ipPort[0],
			Port:       ipPort[1],
			UpdateTime: now,
		}).FirstOrCreate(&raw)

	data := map[string]interface{}{"update_time": now}
	tx.Table("raw_proxies").Where("id=?", raw.ID).Updates(data)

	tx.Commit()
}

type Proxy struct {
	ID                    int64   `gorm:"index;primary_key" json:"id"`
	IP                    string  `json:"ip"`
	Port                  string  `json:"port"`
	Content               string  `gorm:"unique_index:unique_content;" json:"content"`
	AssessTimes           int64   `json:"assess_times"`
	SuccessTimes          int64   `json:"success_times"`
	AvgResponseTime       float64 `json:"avg_response_time"`
	ContinuousFailedTimes int64   `json:"continuous_failed_times"`
	Score                 float64 `json:"score"`
	UpdateTime            int64   `json:"update_time"`
}

func UpdateProxy(db *gorm.DB, Content string, isOK bool, timeCost float64) error {
	tx := db.Begin()
	defer func() {
		if r := recover(); r != nil {
			tx.Rollback()
		}
	}()

	now := time.Now().Unix()

	proxy := &Proxy{}

	if result := tx.First(proxy, &Proxy{Content: Content}); result.Error != nil {
		if result.RecordNotFound() {

			splitData := strings.Split(Content, ":")
			IP, Port := splitData[0], splitData[1]

			proxy = &Proxy{
				IP:                    IP,
				Port:                  Port,
				Content:               Content,
				UpdateTime:            now,
				AssessTimes:           0,
				SuccessTimes:          0,
				AvgResponseTime:       0,
				ContinuousFailedTimes: 0,
				Score:                 1,
			}
			tx.Create(proxy)
		} else {
			return result.Error
		}
	}

	proxy.AssessTimes++
	assessTimes := float64(proxy.AssessTimes)
	proxy.AvgResponseTime = (proxy.AvgResponseTime*(assessTimes-1.0) + timeCost) / assessTimes
	if isOK {
		proxy.ContinuousFailedTimes = 0
		proxy.SuccessTimes++
		log.Printf("[Assessor]Proxy %s assess pass(%gms)", proxy.Content, timeCost)
	} else {
		proxy.ContinuousFailedTimes++
		log.Printf("[Assessor]Proxy %s Assess Failed", proxy.Content)
	}
	proxy.UpdateTime = now
	proxy.Score = proxy.getScore()

	successRate := float64(proxy.SuccessTimes) / float64(proxy.AssessTimes)
	if successRate < AssessorAllowSuccessRateMin {
		log.Printf("[Assessor]Proxy %s Deleted: score too low", proxy.Content)
		tx.Delete(proxy)
	} else {
		tx.Save(proxy)
	}

	tx.Commit()

	return nil
}

func (p *Proxy) getScore() float64 {
	times := float64(p.AssessTimes)
	success := float64(p.SuccessTimes)
	speed := math.Sqrt(float64(RequestTimeout)) / p.AvgResponseTime
	mutation := 1 / math.Pow(float64(p.ContinuousFailedTimes)+1, 2.0)
	score := success * speed * mutation / math.Sqrt(times)

	return score
}

func Migrate(db *gorm.DB) {
	db.AutoMigrate(&RawProxy{}, &Proxy{})
}
