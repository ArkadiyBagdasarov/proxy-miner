package minerWebanetlabs

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	"github.com/jinzhu/gorm"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"net/url"
	"proxy-miner/models"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	baseUrl  = "https://webanetlabs.net"
	proxyUrl = baseUrl + "/publ/24-%d"
	httpbin  = "https://httpbin.org/ip"
	timeout  = models.RequestTimeout * time.Second
)

func Start(db *gorm.DB) {
	for {
		maxPages := getCountPages()
		for i := 1; i < maxPages; i++ {
			links := getLinksFiles(i)
			if links == nil {
				continue
			}

			checkAndCreate(links, db)
		}
	}
}

func checkAndCreate(links []string, db *gorm.DB) {
	wg := &sync.WaitGroup{}
	sem := make(chan bool, 50)

	for _, link := range links {
		rawProxies := getProxyList(link)

		if rawProxies == nil {
			continue
		}

		for _, proxy := range rawProxies {
			wg.Add(1)
			go checkProxy(db, proxy, wg, sem)
		}
	}
	wg.Wait()
}

func checkProxy(db *gorm.DB, proxyContent string, wg *sync.WaitGroup, sem chan bool) {
	defer wg.Done()
	defer func() {
		<-sem
	}()

	sem <- true

	type Resp struct {
		Origin string `json:"origin"`
	}

	u, err := url.Parse("http://" + proxyContent)
	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return
	}

	transport := &http.Transport{
		Proxy: http.ProxyURL(u),
	}

	transport.DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
		conn, err := net.DialTimeout(network, addr, timeout)
		if err != nil {
			return nil, err
		}
		_ = conn.SetDeadline(time.Now().Add(timeout))
		return conn, nil
	}

	client := &http.Client{
		Transport: transport,
	}

	request, err := http.NewRequest("GET", httpbin, nil)
	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return
	}

	response, err := client.Do(request)
	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return
	}

	defer response.Body.Close()

	resp := Resp{}

	decoder := json.NewDecoder(response.Body)
	err = decoder.Decode(&resp)

	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return
	}
	models.CreateRawProxy(db, proxyContent)
}

func getProxyList(link string) []string {
	client := &http.Client{}
	req := getRequest(link)

	if req == nil {
		return nil
	}

	resp, err := client.Do(req)

	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return nil
	}
	defer resp.Body.Close()

	bodyBytes, _ := ioutil.ReadAll(resp.Body)
	bodyString := string(bodyBytes)
	return strings.Split(bodyString, "\r\n")
}

func getCountPages() int {
	selector := "div span.pagesBlockuz2 a:last-child"

	client := &http.Client{}
	req := getRequest(fmt.Sprintf(proxyUrl, 1))

	if req == nil {
		return 0
	}

	resp, err := client.Do(req)

	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return 0
	}
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return 0
	}

	countPagesNode := doc.Find(selector).First()

	countPages := 1

	href, ok := countPagesNode.Attr("href")
	if ok {
		countPages, _ = strconv.Atoi(strings.Split(href, "-")[1])
	}

	return countPages
}

func getLinksFiles(page int) []string {
	client := &http.Client{}
	req := getRequest(fmt.Sprintf(proxyUrl, page))

	if req == nil {
		return nil
	}

	resp, err := client.Do(req)

	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return nil
	}
	defer resp.Body.Close()

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return nil
	}

	links := []string{}

	doc.Find("a.link").Each(func(i int, s *goquery.Selection) {
		href, ok := s.Attr("href")
		if ok {
			links = append(links, baseUrl+href)
		}
	})

	return links
}

func getRequest(link string) *http.Request {
	req, err := http.NewRequest("GET", link, nil)

	if err != nil {
		log.Printf("[Miner][%s] %s", baseUrl, err)
		return nil
	}

	req.Header.Add("User-Agent", models.UserAgent)
	return req
}
