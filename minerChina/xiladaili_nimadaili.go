package minerChina

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gocolly/colly"
	"github.com/jinzhu/gorm"
	"log"
	"math/rand"
	"net"
	"net/http"
	"net/url"
	"proxy-miner/models"
	"sync"
	"time"
)

const (
	proxyUrl = "%s/%s/?page=%d"
	httpbin  = "https://httpbin.org/ip"
	timeout  = models.RequestTimeout * time.Second
)

func Start(db *gorm.DB) {
	for {

		wg := &sync.WaitGroup{}
		go checkAndCreate("http://www.xiladaili.com", "http", db, wg)
		go checkAndCreate("http://www.xiladaili.com", "https", db, wg)

		go checkAndCreate("http://www.nimadaili.com", "http", db, wg)
		go checkAndCreate("http://www.nimadaili.com", "https", db, wg)
		wg.Add(4)
		wg.Wait()

		time.Sleep(time.Minute)
	}
}

func checkAndCreate(siteUrl string, typeProxy string, db *gorm.DB, wg *sync.WaitGroup) {
	defer wg.Done()

	c := colly.NewCollector(
		colly.UserAgent(models.UserAgent),
		colly.Async(true),
	)
	c.SetRequestTimeout(timeout)

	_ = c.Limit(&colly.LimitRule{
		Parallelism: 15,
	})

	c.OnError(func(_ *colly.Response, err error) {
		log.Printf("[Miner][%s]Visit error: %s", siteUrl, err)
	})
	c.OnRequest(func(request *colly.Request) {
		log.Printf("[Miner][%s]Start visit: %s", siteUrl, request.URL)
	})
	c.OnHTML(".fl-table tr", func(element *colly.HTMLElement) {
		item := element.DOM
		content := item.Find("td:nth-child(1)").Text()

		if content == "" {
			return
		}

		go func(d *gorm.DB) {
			err := doRequest(d, content)

			if err != nil {
				log.Printf("[Miner][%s] %s", siteUrl, err)
			} else {
				log.Printf("[Miner][%s]Proxy %s was mined", siteUrl, content)
			}
		}(db)
	})
	c.OnScraped(func(response *colly.Response) {
		log.Printf("[Miner][%s]Finish visit: %s", siteUrl, response.Request.URL)
	})

	for i := 1; i < 2000; i++ {
		err := c.Visit(fmt.Sprintf(proxyUrl, siteUrl, typeProxy, i))
		c.Wait()

		if err != nil {
			log.Println(err)
		}
		sleep()
	}
}

func sleep() {
	delay := 3 + rand.Intn(5)
	time.Sleep(time.Duration(delay) * time.Second)
}

func doRequest(db *gorm.DB, proxyContent string) error {
	type Resp struct {
		Origin string `json:"origin"`
	}

	u, err := url.Parse("http://" + proxyContent)
	if err != nil {

		return err
	}

	transport := &http.Transport{
		Proxy: http.ProxyURL(u),
	}

	transport.DialContext = func(ctx context.Context, network, addr string) (net.Conn, error) {
		conn, err := net.DialTimeout(network, addr, timeout)
		if err != nil {
			return nil, err
		}
		_ = conn.SetDeadline(time.Now().Add(timeout))
		return conn, nil
	}

	client := &http.Client{
		Transport: transport,
	}

	request, err := http.NewRequest("GET", httpbin, nil)
	if err != nil {
		return err
	}

	response, err := client.Do(request)
	if err != nil {
		return err
	}

	defer response.Body.Close()

	resp := Resp{}

	decoder := json.NewDecoder(response.Body)
	err = decoder.Decode(&resp)

	if err != nil {
		return err
	}
	models.CreateRawProxy(db, proxyContent)

	return nil
}
