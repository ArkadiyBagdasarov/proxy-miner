package main

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"os"
	"proxy-miner/models"
)

var (
	//host     = "127.0.0.1" //os.Getenv("host")
	//port     = "5432"      // os.Getenv("port")
	//user     = "proxy"     //os.Getenv("user")
	//dbname   = "proxy"     //os.Getenv("dbname")
	//password = "ghbdtn"    //os.Getenv("password")

	host     = os.Getenv("host")
	port     = os.Getenv("port")
	user     = os.Getenv("user")
	dbname   = os.Getenv("dbname")
	password = os.Getenv("password")
)

func NewPostgresql() (db *gorm.DB, err error) {
	dbString := "host=%s port=%s user=%s dbname=%s password=%s"
	pgConf := fmt.Sprintf(dbString, host, port, user, dbname, password)

	db, err = gorm.Open("postgres", pgConf)
	if err != nil {
		return
	}

	models.Migrate(db)

	return db, nil
}
